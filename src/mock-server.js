import { Server, Faker, uid } from 'react-mock';
 
const apiRoute = '/api/v1/users';
const requestHandler = (request, generator) => {
  const users = generator.next(10, schema);
  const addresses = generator.next(10, addressSchema);
  for (let i=0; i<users.length; i++) {
    let address = addresses[i];
    delete address['id'];
    users[i].address = address;
  };
  return [200, { 'Content-Type': 'application/json' }, JSON.stringify(users)];
}


const genders = [ 'female' , 'male' ];

const addressSchema = {
    number: () => Faker.random.number(),
    street: () => Faker.address.streetName(),
    city: () => Faker.address.city(),
    zipcode: () => Faker.address.zipCode()
}

 
const schema = {
  name: () => Faker.name.findName(),
  id: () => uid.next(),
  gender: () => Faker.random.arrayElement(genders),
  email: () => Faker.internet.email()  
};
 
const errorFormat = {
  message: Faker.lorem.sentence()
}
 
Server.mockGet(apiRoute, requestHandler, schema, errorFormat)

 
Server.on() // @returns Promise that resolves with null or rejects with Error
// Server.off()
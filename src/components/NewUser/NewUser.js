import React, { Component } from 'react';
import {uid} from 'react-mock';

import TextField from '@material-ui/core/TextField';
import { SelectField } from 'react-md';

import Button from '@material-ui/core/Button';


import './NewUser.css';

class NewUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            gender:'',
            email:'',
            phone:'',
            address_city:'',
            address_number:'',
            address_street:'',
            address_zipcode:'',
            formErrors: {firstName:'First Name is required', lastName:'Last Name is required', gender:'Please, select gender', email: 'Email is not valid', phone:'Phone is not valid (try changing the format to +XX X XXXXXXXX)', address_city:'City is required', address_number:'House number is required', address_street:'Street is required', address_zipcode:'Zipcode is required'},
            validations: {
                emailValid: false,
                firstNameValid: false,
                lastNameValid: false,
                genderValid: false,
                phoneValid: false,
                address_cityValid: false,
                address_numberValid: false,
                address_streetValid: false,
                address_zipcodeValid: false,
                formValid: false
            },
            touched: {
                email: false,
                firstName: false,
                lastName: false,
                gender: false,
                phone: false,
                address_city: false,
                address_number: false,
                address_street: false,
                address_zipcode: false               
            },
            
            showErrors:false
        }

        this.handleReset = this.handleInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInput = this.handleInput.bind(this);
    }

    handleSubmit(evt) {
        const user = {
            name: `${this.state.firstName} ${this.state.lastName}`,
            email: this.state.email,
            gender: this.state.gender,
            phone: this.state.phone,
            id: uid.next(),
            address: {
                number: this.state.address_number,
                street: this.state.address_street,
                city: this.state.address_city,
                zipcode: this.state.address_zipcode
            }
        };
        this.props.sendUser(user);
        evt.preventDefault();
    }

    handleInput(evt) {
        console.log(evt);
        const name = evt.target.name;
        const value = evt.target.value;
        this.setState({ [name]: value }, () => { this.validateField(name, value) });
    }


    handleBlur = (field) => (evt) => {
        this.setState({
          touched: { ...this.state.touched, [field]: true },
        });
    }

    validateField(fieldName, value) {
        let emailValid = this.state.validations.emailValid;
        let firstNameValid = this.state.validations.firstNameValid;
        let lastNameValid = this.state.validations.lastNameValid;
        let genderValid = this.state.validations.genderValid;
        let phoneValid = this.state.validations.phoneValid;
        let address_cityValid = this.state.validations.address_cityValid;
        let address_numberValid = this.state.validations.address_numberValid;
        let address_streetValid = this.state.validations.address_streetValid;
        let address_zipcodeValid = this.state.validations.address_zipcodeValid;
      switch(fieldName) {
          case 'email':
            emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
            break;
          case 'firstName':
            firstNameValid = value != '';
            break;
        case 'lastName':
            lastNameValid = value != '';
            break;
        case 'gender':
            genderValid = value === 'male' || value === 'female' || value === 'other';
            break;
        case 'phone':
            phoneValid = value.match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im);
            break;
        case 'address_city':
            address_cityValid = value != '';
            break;
        case 'address_street':
            address_streetValid = value != '';
            break;
        case 'address_zipcode':
            address_zipcodeValid = value != '';
            break;
        case 'address_number':
            address_numberValid = value != '';
            break;
          default:
            break;
        }
        this.setState({ validations: {emailValid: emailValid,
                        firstNameValid:firstNameValid,
                        lastNameValid:lastNameValid,
                        genderValid:genderValid,
                        phoneValid:phoneValid,
                        address_cityValid:address_cityValid,
                        address_numberValid:address_numberValid,
                        address_streetValid:address_streetValid,
                        address_zipcodeValid:address_zipcodeValid}
                      }, this.validateForm);
      }

      validateForm() {
        this.setState({formValid: this.state.validations.emailValid &&
            this.state.validations.firstNameValid &&
            this.state.validations.lastNameValid &&
            this.state.validations.genderValid &&
            this.state.validations.phoneValid &&
            this.state.validations.address_cityValid &&
            this.state.validations.address_numberValid &&
            this.state.validations.address_streetValid &&
            this.state.validations.address_zipcodeValid});        
      }



    render() {
        const fieldLabels = [{name: 'firstName', label: 'First Name', validation:'firstNameValid' }, {name: 'lastName', label: 'Last Name', validation:'lastNameValid' }, {name: 'gender', label: 'Gender', validation:'genderValid' }, {name: 'email', label: 'Email', validation:'emailValid' }, {name: 'phone', label: 'Phone', validation:'phoneValid' }, {name: 'address_number', label: 'House Number', validation:'address_numberValid' }, {name: 'address_street', label: 'Street', validation:'address_streetValid' }, {name: 'address_city', label: 'City', validation:'address_cityValid' }, {name: 'address_zipcode', label: 'Zipcode', validation:'address_zipcodeValid' }];
        const formFields = fieldLabels.map((label) => {
           
              return(
                  <div key={label.name}>
                      <TextField style={{width:'100%'}}
                            onBlur={this.handleBlur(label.name)}
                              required
                              error = {!this.state.validations[label.validation] && this.state.touched[label.name]}
                              helperText={!this.state.validations[label.validation] ? this.state.formErrors[label.name] : ''}
                              name={label.name}
                              label={label.label}
                              value={this.state[label.name]}
                              onChange={this.handleInput}
                      />
                  </div>
              )
            

        });

        return(
            <div>
                <h3>Add New User</h3>
                <form style={{padding:20}} onSubmit={this.handleSubmit} noValidate autoComplete="off">
                    { formFields }
                    <div style={{padding:20}}>
                    <Button variant="contained" type="submit" disabled={!this.state.formValid}>Add User</Button>                    
                    </div>
                </form>
            </div>
        )
    }

}

export default NewUser;

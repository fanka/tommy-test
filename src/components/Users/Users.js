import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import NewUser from '../NewUser/NewUser';
import DeleteIcon from '@material-ui/icons/Delete';
import './Users.css';

class Users extends Component {

    constructor(props) {
        super(props);
        this.state = {
            users: []
        }

        this.getNewUser = this.getNewUser.bind(this);
    }

    getNewUser(user){
        // do not forget to bind getData in constructor
        this.setState({
            users:this.state.users.concat(user)
        })
    }


    componentDidMount() {
        axios.get('/api/v1/users')
            .then((response) => {
                this.setState({
                    users: response.data
                });
                console.log(this.state.users);
            })
    }



    removeUser(userId) {
        this.setState({users: this.state.users.filter(function(user) {
            return user.id !== userId
        })});
    }
    render() {
        let usersList = this.state.users.map((user) => {
            return(<Paper key={user.id} style={{marginTop: 10}} className="pt-2 pb-2">
            {user.name} <DeleteIcon className="float-right" onClick={()=> this.removeUser(user.id)}></DeleteIcon>
            </Paper>)
        })
        return(
                    <div className="row">
                        <div className="col-md-6">
                          <h3>Users</h3>
                          {usersList}
                        </div>
                        <div className="col-md-6">
                          <NewUser sendUser={this.getNewUser} />
                        </div>
                    </div>
        )

    }
}

export default Users;

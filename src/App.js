import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import Users from './components/Users/Users';
import mockServer from './mock-server';
import Grid from '@material-ui/core/Grid';
import NewUser from './components/NewUser/NewUser';

class App extends Component {


  render() {
    return (
      <div className="App">
        <div className="container">
            <Users />
        </div>
      </div>
    );
  }
}



export default App;

Sorry, I really didn't have time last week and didn't see the email about postponing the deadline to Monday :-(

  But I still decided to spend my lunch time today and build at least something. So I've set up a very simple web app in React without any real styling and refactoring. You can:
  - view the list of users (now I randomly generate 10 users with mocking data package)
  - remove any user by clicking the icon
  - add user by submitting the form in the bottom (now I only add the user to the state and display in the list, in real life I would also make a post request and update info on the server). If I had more time, I would also make the gender field type select, not text.
  - I also implemented some validations for the form (all the fields are required, email and phone are checked for validity).
  - In the build folder there's a production build.

I hope that next time I'll have a chance to show myself better. But still wanted to give it a try. 

